package edu.purdue.cs555;

import java.io.*;
import java.util.*;
import Jama.*;
import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import com.sun.org.apache.xml.internal.security.utils.Base64;

/**
 * This class provides graph processing helper functions.
 *
 */
public class Graph implements Serializable{
	private static final long serialVersionUID = 1L;
	private long numberOfNodes;
	private Matrix adjacencyMatrix;
    private SecretKeySpec[][] commitmentKeys;
	private String[][] commitment;

    public Graph(){}
	public Graph( Matrix adjacencyMatrix){
		this.adjacencyMatrix = adjacencyMatrix;
	}
	public Graph( String[][] commitments ){
		this.commitment = commitments;
	}

	/**
	 * This method takes the path to a graph fue and ports it to a graph object.
	 */
	public long importGraph(String graphFileName){
		BufferedReader bufferedReader = null;
		int rows = 0;
		this.numberOfNodes = 0;
		double[][] adj_mat;
		try{
			bufferedReader = new BufferedReader( new FileReader(graphFileName));
			while (bufferedReader.readLine() != null) this.numberOfNodes++;
			bufferedReader.close();
			adj_mat= new double [(int)this.numberOfNodes][(int)this.numberOfNodes];
			bufferedReader = new BufferedReader( new FileReader(graphFileName));
			String line;
			String[] elements;
			while((line = bufferedReader.readLine()) != null){
				elements = line.split(" ");
				for(int i=0; i< this.numberOfNodes; i++){
					if( elements[i].equals("0") ){
						adj_mat[rows][i] = Double.parseDouble("0");
					}else{
						adj_mat[rows][i] = Double.parseDouble("1");
					}
				}
				rows++;
			}
            this.adjacencyMatrix = new Matrix(adj_mat);
		}catch(Exception e ){
			e.printStackTrace();
		} finally {
			try{
				if(bufferedReader != null ){
					bufferedReader.close();
				}
			}catch(IOException e){}
		}
		return this.numberOfNodes;
	}

	/**
	 * This method simply returns the corresponding adjacent matrix
	 * of the graph.
	 */
	public Matrix getMat(){
		return this.adjacencyMatrix;
	}

	/**
	 * This method returns an isomorphic graph of the calling  graph.
	 * By making use of the adjacency matrix and provided permutation
	 * graph.
	 */
	public Graph getIsomorphicMat( Graph P ){
		Matrix P_t = P.getMat().transpose();
		Matrix tmp = P.getMat().times(adjacencyMatrix);
		return new Graph( tmp.times(P_t) );
	}

	/**
	 * This method creates and returns a random permutation graph
	 * the same size as  number of rows argument passed. This graph
	 * serves as the permutation function.
	 */
	public Graph createPermutationMat(int numberOfRows){
		Matrix P = new Matrix(new double [numberOfRows][numberOfRows]);
		Map<Integer , String> positions = new HashMap<Integer, String>();
		Map<Integer , String> rows = new HashMap<Integer, String>();

		for (int i = 0; i< numberOfRows; i++){
			if(rows.get(i) ==null){
				while (true){
					int rand = (int)  (Math.random()* (numberOfRows));
					if( rand == numberOfRows){
						rand = numberOfRows -1;
					}
					if(positions.get(rand) == null && rows.get(rand) == null){
						P.set(i,rand,1.0);
						P.set(rand,i,1.0);
						positions.put(rand,"1");
						rows.put(i,"1");
						rows.put(rand,"1");
						break;
					}
				}
			}
		}
		return new Graph(P);
	}

	/**
	 * This method creates and returns a random permutation of the calling
	 * graph using the permutation graph passed in as an argument.
	 */
	public Graph createPermutationMat( Graph p ){
		return new Graph( adjacencyMatrix.times(p.getMat()));
	}

	/**
	 * This method simply returns the number ofrows of the adjacency matrix used
	 * of the calling object
	 */
	 public int getNumRows(){
		 return adjacencyMatrix.getRowDimension();
	 }

	/**
	 * This method simply returns the byte array equivalent of the adjacency matrix
	 * of the calling graph.
	 */
	 public byte[] getMatValueBytes(int i, int j){
		 ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
		 DataOutputStream dataOutputStream = new DataOutputStream(byteArrayOutputStream);
		 try {
			 dataOutputStream.writeDouble(adjacencyMatrix.get(i, j));
		 } catch (IOException e) {
			 e.printStackTrace();
		 }
		 return byteArrayOutputStream.toByteArray();
	 }

	/**
	 * This method returns a double corresponding to the
	 * byte array passed as an input argument.
	 */
	 public double bytesToDouble( byte[] b){
		 ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(b);
		 DataInputStream dataInputStream = new DataInputStream( byteArrayInputStream );
		 try {
			 return dataInputStream.readDouble();
		 } catch (IOException e) {
			 e.printStackTrace();
		 }
		 return 0;
	 }

	/**
	 * This method creates a encrypted commitment of the
	 * byte array calling object (graph) and stores this
	 * commitment in the commitment attribute of the calling
	 * graph.
	 */
	 public void createCommitment(){
		 Cipher cipher, decipher;
		 Random r = new Random();
         int length = adjacencyMatrix.getArray().length;
		 byte key_value[] = new byte[16];
		 commitmentKeys = new SecretKeySpec[length][length];
		 commitment = new String[length][length];
		 adjacencyMatrix.print(1, 1);
		 try{
			 for(int i=0; i < length; i++){
				 for(int j=0; j < length; j++){
					 r.nextBytes(key_value);
					 commitmentKeys[i][j] = new SecretKeySpec(key_value, "AES");
                     cipher = Cipher.getInstance(Strings.AES_PADDING);
					 cipher.init(Cipher.ENCRYPT_MODE, commitmentKeys[i][j]);
					 byte[] cipherText = cipher.doFinal( getMatValueBytes(i, j));
					 commitment[i][j] = Base64.encode(cipherText);
					 decipher = Cipher.getInstance(Strings.AES_PADDING);
					 decipher.init(Cipher.DECRYPT_MODE, commitmentKeys[i][j]);
				 }
			 }
		 }catch(Exception e){
			 e.printStackTrace();
		 }
	 }

	/**
	 * This method decrypts the encrypted commitment matrix of
	 * the calling graph
	 */
	 public void decrypt(SecretKeySpec[][] decryp_keys){
		 adjacencyMatrix = new Matrix(commitment.length, commitment.length);
		 Cipher decrypt;
		 int length = decryp_keys.length;
		 try{
			 for(int i=0; i < length; i++){
				 for(int j=0; j < length; j++){
					 if( decryp_keys[i][j] != null ){
						 decrypt = Cipher.getInstance(Strings.AES_PADDING);
						 decrypt.init(Cipher.DECRYPT_MODE, decryp_keys[i][j]);
						 byte[] results = decrypt.doFinal( Base64.decode(commitment[i][j]));
						 adjacencyMatrix.set(i, j, bytesToDouble(results));
					 }
				 }
			 }
		 }catch(Exception e){
			 e.printStackTrace();
		 }
	 }

	/**
	 * This method returns a matrix of decryption keys for
	 * specific positions in the encrypted commitment matrix
	 * pertaining to the underlying subgraph, this is used to
	 * prove knowledge of the subgraph
	 */
	 public SecretKeySpec[][] getKeysForSubgraph(Graph subGraph){
		 SecretKeySpec sub_keys[][] = new SecretKeySpec[adjacencyMatrix.getRowDimension()][adjacencyMatrix.getRowDimension()];
		 Matrix sub_mat = subGraph.getMat();
		 for(int i=0; i < adjacencyMatrix.getRowDimension(); i++){
			 for(int j=0; j< adjacencyMatrix.getRowDimension(); j++){
				 if( sub_mat.get(i, j) == 1){
					 sub_keys[i][j] = commitmentKeys[i][j];
				 }else{
					 sub_keys[i][j] = null;
				 }
			 }
		 }
		 return sub_keys;
	 }

	/**
	 * This method returns a matrix of decryption keys for
	 * for the commitment matrix of the calling Graph object.
	 */
	public SecretKeySpec[][] getCommitmentKeys(){
		return commitmentKeys;
	 }

	/**
	 * This method compares the adjacency matrix of the calling graph
	 * and the graph provided as an argument, its the comparison that
	 * is used to verify knowledge of the secret.
	 */
	public boolean compare(Graph gLarge, boolean failSafe) {
		Matrix gLargeMat = gLarge.getMat();
		for(int i=0; i < adjacencyMatrix.getRowDimension(); i++){
			 for(int j=0; j< adjacencyMatrix.getRowDimension(); j++){
				if( gLargeMat.get(i, j) != adjacencyMatrix.get(i,j)){
					return false | failSafe;
				}
			 }
		}
		return true;
	}

	/**
	 * This method simply returns the commitment matrix.
	 */
	public String[][] getCommitment(){
		return commitment;
	}
}
