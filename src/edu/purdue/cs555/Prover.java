package edu.purdue.cs555;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;

/**
 * This class provides prover functionality, in this 
 * class a challenge is received from the verifier, 
 * the prover provides an encrypted commitment graph, 
 * keys to decrypt some verticies in the graph, and the 
 * permutation graph.  The prover class is run as a secondary
 * thread in order to seperate concerns from painting GUI and 
 * handling user events.
 */
public class Prover extends Thread{
	TestClient testClient = new TestClient();
	private Socket socket;
	private BufferedReader inFromVerifier;
	private ObjectOutputStream objectOutput;
	private Graph largeGraph;
	private Graph smallGraph;
	private Graph permutation;
	private Graph subGraph;
	private Graph randomPermutationGraph;
	private Graph permutationOfLargeToSmallGraph;
	private Graph permutationOflargeGraph;
	private Graph permutationOfSmallGraph;
	private String log = new String();

	/**
	 * This constructor receives paths to both graphs, the 
	 * permutation function, the verifier's hostname and port
	 * number. Class variables are set using these arguments.
	 */
	public Prover( String largeGraphFile,
		String smallGraphFile,
		String permutationFile,
		String verifierHost,
		int verifierPort ){
		this.largeGraph = new Graph();
		this.largeGraph.importGraph(largeGraphFile);
		this.smallGraph = new Graph();
		this.smallGraph.importGraph(smallGraphFile);
		this.permutation = new Graph();
		this.permutation.importGraph(permutationFile);
		this.subGraph = smallGraph.getIsomorphicMat(permutation);
		try{
			socket = new Socket(verifierHost, verifierPort);
			objectOutput = new ObjectOutputStream(socket.getOutputStream());
			inFromVerifier = new BufferedReader(new InputStreamReader(socket.getInputStream()));
		} catch (UnknownHostException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * This method creates and configures the objects used to answer
	 *  the verifier's challenges.
	 */
	public void createChallengeObjects(){
		randomPermutationGraph = permutationOfLargeToSmallGraph = permutationOflargeGraph = permutationOfSmallGraph = null;
		randomPermutationGraph = largeGraph.createPermutationMat(largeGraph.getNumRows());
		permutationOflargeGraph = largeGraph.getIsomorphicMat(randomPermutationGraph);
		permutationOflargeGraph.createCommitment();
		permutationOfSmallGraph = subGraph.getIsomorphicMat(randomPermutationGraph);
		permutationOfLargeToSmallGraph = permutation.createPermutationMat(randomPermutationGraph);
	}

	/**
	 * This method contains the logic to answer the verifier's challenges,
	 * it receives the challenge from the verifier application and uses the
	 * value to determine which answer to provide.  It then waits for a res-
	 * ponse from the verifier and displays the result in the provers Gui.
	 */
	public boolean answerChallenge(){
		String challengeFromVerifier = new String();
		try {
			Graph commit = new Graph( permutationOflargeGraph.getCommitment());
			String verifier_challenge = inFromVerifier.readLine();
			this.testClient.setupProverGUI();
			Thread.sleep(10);
			this.testClient.notificationLabel.setText(Strings.RESPONSE_PENDING_MSG);
			Thread.sleep(10);
			if( verifier_challenge.equals("1")){
				ArrayList<Object> objects = new ArrayList<Object>();
				objects.add(commit);
				objects.add(randomPermutationGraph);
				objects.add(permutationOflargeGraph.getCommitmentKeys());
				objectOutput.writeObject(objects);
			}else{
				ArrayList<Object> objects = new ArrayList<Object>();
				objects.add(commit);
				objects.add(permutationOfLargeToSmallGraph);
				objects.add(permutationOflargeGraph.getKeysForSubgraph(permutationOfSmallGraph));
				objectOutput.writeObject(objects);
			}
			challengeFromVerifier = inFromVerifier.readLine();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		this.testClient.notificationLabel.setText(Strings.RESPONSE_RCVD_MSG);
		if( !challengeFromVerifier.equalsIgnoreCase(Strings.OK)){
			log += Strings.FAILED+"\n";
			TestClient.proverEditorPane.setText(log);
			return false;
		}else{
			log += Strings.PASSED+"\n";
			TestClient.proverEditorPane.setText(log);
			return true;
		}
	}

	/**
	 * This method  continuously runs the prover setup methods  and keeps 
	 * the thread aliveuntil the userdoes not want to continue trying to 
	 * verifier knowledge of the secrets.
	 */
	public void run(){
		createChallengeObjects();
		answerChallenge();
		while(true){
			if(testClient.reInitialize() == TestClient.RetryOption.YES){
				createChallengeObjects();
				answerChallenge();
			}
			else break;
		}
		System.exit(0);
	}
}
