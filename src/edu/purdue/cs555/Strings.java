package edu.purdue.cs555;

/**
 * This class contains only string literal constants used mainly 
 * for GUI namesas well as other string constants throughout the 
 * application. The purpose of this classes is to provide a central 
 * place for changing the names of different components improving 
 *  maintainability. It also allows easier translation to different 
 * languages.
 */
public  class Strings {
    public static final String VERIFIER_TITLE = "Verifier";
    public static final String PROVER_TITLE = "Prover";
    public static final String START_PROVER_TITLE = "Start Prover";
    public static final String START_VERIFIER_TITLE = "Start Verifier";
    public static final String HOST_TITLE = "Host Name";
    public static final String HOST_PLACEHOLDER = "   Hostname eg:localhost";
    public static final String VERIFIER_LABEL = "Set Up Port and Graph Files";
    public static final String PORT_NUMBER_LABEL = "port #";
    public static final String GRAPH_ONE_FIELD_DEFAULT_TEXT = " add path to graph 1";
    public static final String GRAPH_ONE_FIELD_DEFAULT_NAME = "graph1path";
    public static final String GRAPH_TWO_FIELD_DEFAULT_TEXT = " add path to graph 2";
    public static final String GRAPH_TWO_FIELD_DEFAULT_NAME = "graph2path";
    public static final String GRAPH_PI_FIELD_DEFAULT_TEXT = " add path to Pi source file";
    public static final String OPEN = "open";
    public static final String OK = "OK";
    public static final String NOT_OK = "NOTOK";
    public static final String GRAPH_ONE_BUTTON_NAME = "graph1open";
    public static final String GRAPH_TWO_BUTTON_NAME = "graph2open";
    public static final String PI_BUTTON_NAME = "piopen";
    public static final String PROVER_OR_VERIFIER = "Prover or Verifier";
    public static final String CONNECTION_PENDING = "Waiting for connection";
    public static final String CONNECTION_INITIATED = "Initiated Connection, Waiting for Challenge";
    public static final String CHOOSE_G1 = "Choose graph 1";
    public static final String CHOOSE_G2 = "Choose graph 2";
    public static final String CHOOSE_PI = "Choose pi permutation file";
    public static final String VERIFIER_OK = "OKVerifier";
    public static final String PROVER_OK = "OKProver";
    public static final String CHALLENGE_ONE_OPTION = "Show Challenge 1";
    public static final String CHALLENGE_TWO_OPTION = "Show Challenge 2";
    public static final String PROVER_REQUEST = "Got prover request";
    public static final String CHALLENGE_RCVD = "Challenge Received...";
    public static final String SEND_BUTTON_TITTLE = "Send";
    public static final String PROVER_RESPONSE_PLACEHOLDER = "**VERIFIER RESPONSE WILL SHOW UP HERE**";
    public static final String VERIFIER_RESPONSE_PLACEHOLDER = "\"**RECEIVED PROVER REQUEST**\\n\\nRESULT OF CHALLENGE RESPONSE WILL SHOW UP HERE....\"\n";
    public static final String TRY_AGAIN_MSG = "Would you like to try again?";
    public static final String PROVER_BUTTON_NAME = "Prover";
    public static final String VERIFIER_BUTTON_NAME = "Verifier";
    public static final String SEND_CHALLENGE_BUTTON_NAME = "SendChallenge";
    public static final String AES_PADDING = "AES/ECB/PKCS5Padding";
    public static final String RESPONSE_PENDING_MSG = "Challenge Answered, awaiting response";
    public static final String RESPONSE_RCVD_MSG = "Verifier response received";
    public static final String PASSED = "VERIFICATION PASSED";
    public static final String FAILED = "VERIFICATION FAILED";
}

