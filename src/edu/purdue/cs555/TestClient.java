package edu.purdue.cs555;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

/**
 * This class pertains to the main user
 * interface of the application. It handles
 * updating the user interface and serves as
 * a proxy between the prover and the verifier.
 * I starts the prover and verifier threads when
 * the user choses which app they want to launch.
 */
public  class TestClient{

    public static JComboBox challengeBox;
    public static JEditorPane verifierEditorPane;
    public static JEditorPane proverEditorPane;
    public static JLabel notificationLabel =  new JLabel();
    private static Prover prover;
    private static Thread proverThread;
    private static Thread verifierThread;
    private static Verifier verifier;
    private static JTextField graph1Field;
    private static JTextField graph2Field;
    private static JTextField piField;
    private static JTextField hostField;
    private static JTextField portField;
    private static JPanel panel = new JPanel();
    private static JFrame frame = new JFrame(Strings.PROVER_OR_VERIFIER);

    public enum Option {
       PROVER, VERIFIER
   }
    public enum RetryOption {
        YES, NO
    }
    public TestClient(){}

    /**
     * This method initiates a prover or verifier thread together with
     * the initial GUI, depending on the 'option' argument.
     */
    private  void connectionInitiation(String hostname, int portNumber, Option option){
        switch (option){
            case VERIFIER:
                verifier = new Verifier(graph2Field.getText(), graph1Field.getText(), portNumber);
                verifier.testClient = this;
                panel.removeAll();
                frame.dispose();
                frame = new JFrame(Strings.VERIFIER_TITLE);
                verifier.testClient = this;
                frame.setVisible(true);
                frame.setSize(300, 50);
                frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                notificationLabel.setText(Strings.CONNECTION_PENDING);
                panel.setLayout(new FlowLayout());
                panel.add(notificationLabel);
                frame.add(panel);
                panel.revalidate();
                verifierThread = new Thread(verifier);
                verifierThread.start();
                break;
            case PROVER:
                prover = new Prover( graph2Field.getText(), graph1Field.getText(),piField.getText(), hostname, portNumber);
                panel.removeAll();
                frame.dispose();
                frame = new JFrame(Strings.PROVER_TITLE);
                frame.setTitle(Strings.PROVER_TITLE);
                prover.testClient = this;
                frame.setVisible(true);
                frame.setSize(300,50);
                frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                notificationLabel.setText(Strings.CONNECTION_INITIATED);
                panel.setLayout(new FlowLayout());
                panel.add(notificationLabel);
                frame.add(panel);
                panel.revalidate();
                proverThread = new Thread(prover);
                proverThread.start();
        }
    }

    /**
     * This is the main method, it paints the first GUI container that
     * the user interacts with, here the user chooses to start a prover
     * or a verifier.
     */
    public static void main(String args[]){
        frame.setVisible(true);
        frame.setSize(200,200);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.add(panel);
        JButton proverButton = new JButton(Strings.START_PROVER_TITLE);
        proverButton.setName(Strings.PROVER_BUTTON_NAME);
        panel.add(proverButton);
        proverButton.addActionListener (new buttonListener());
        JButton verifierButton = new JButton(Strings.START_VERIFIER_TITLE);
        verifierButton.setName(Strings.VERIFIER_BUTTON_NAME);
        panel.add(verifierButton);
        verifierButton.addActionListener (new buttonListener());
    }

    /**
     * This class provides an action listener for different buttons
     * that occur during the application's flow. Here we identify a button
     * component by its name and take action based on the button that
     * was pressed.
     */
    static class buttonListener implements ActionListener {
        public void actionPerformed (ActionEvent e) {
            JButton clicked = (JButton)e.getSource();
            if(clicked.getName().compareToIgnoreCase(Strings.PROVER_BUTTON_NAME) == 0){
                TestClient t = new TestClient();
                t.initProver();
            }
            else if(clicked.getName().compareToIgnoreCase(Strings.VERIFIER_BUTTON_NAME) == 0){
                TestClient t = new TestClient();
                t.initVerifier();
            }
            else if(clicked.getName().compareToIgnoreCase(Strings.SEND_CHALLENGE_BUTTON_NAME) == 0){
                verifier.sendChallenge(challengeBox.getSelectedIndex());
            }
            else if(clicked.getName().compareToIgnoreCase(Strings.GRAPH_ONE_BUTTON_NAME) == 0){
                JFileChooser jfc = new JFileChooser();
                jfc.setDialogTitle(Strings.CHOOSE_G1);
                jfc.showOpenDialog(null);
                graph1Field.setForeground(Color.black);
                graph1Field.setText(jfc.getSelectedFile().getAbsolutePath());
            }
            else if(clicked.getName().compareToIgnoreCase(Strings.GRAPH_TWO_BUTTON_NAME) == 0){
                JFileChooser jfc = new JFileChooser();
                jfc.setDialogTitle(Strings.CHOOSE_G2);
                jfc.showOpenDialog(null);
                graph2Field.setForeground(Color.black);
                graph2Field.setText(jfc.getSelectedFile().getAbsolutePath());
            }
            else if(clicked.getName().compareToIgnoreCase(Strings.PI_BUTTON_NAME) == 0){
                JFileChooser jfc = new JFileChooser();
                jfc.setDialogTitle(Strings.CHOOSE_PI);
                jfc.showOpenDialog(null);
                piField.setForeground(Color.black);
                piField.setText(jfc.getSelectedFile().getAbsolutePath());
            }
            else if(clicked.getName().compareToIgnoreCase(Strings.VERIFIER_OK) == 0){
                TestClient t = new TestClient();
                t.connectionInitiation(Strings.HOST_PLACEHOLDER, Integer.parseInt(portField.getText()), Option.VERIFIER);
            }
            else if(clicked.getName().compareToIgnoreCase(Strings.PROVER_OK) == 0){
                TestClient t = new TestClient();
                t.connectionInitiation(hostField.getText(), Integer.parseInt(portField.getText()), Option.PROVER);
            }
        }
    }

    /**
     * This method repaints the verifier GUI once all the
     * fields are filled and Ok button is clikced. The
     * gui then waits for a prover and subsequently is updated
     * after every challenge.
     */
    public void setupVerifierGUI(){
        String [] challanges = {Strings.CHALLENGE_ONE_OPTION,Strings.CHALLENGE_TWO_OPTION};
        this.panel.removeAll();
        JPanel panel1 = new JPanel();
        frame.setResizable(false);
        frame.setPreferredSize(new Dimension(300, 300));
        panel.setLayout(new GridBagLayout());
        panel1.setLayout(new GridBagLayout());
        frame.getContentPane().add(panel, BorderLayout.NORTH);
        GridBagConstraints top = new GridBagConstraints();
        top.anchor = GridBagConstraints.FIRST_LINE_START;
        GridBagConstraints bottom = new GridBagConstraints();
        top.weightx = 1.0;
        top.weighty =1;
        bottom.weighty = 0.5;
        bottom.weightx =1.0;
        bottom.anchor = GridBagConstraints.FIRST_LINE_START;
        bottom.fill = GridBagConstraints.HORIZONTAL;
        bottom.gridwidth = GridBagConstraints.REMAINDER;
        this.notificationLabel.setText(Strings.PROVER_REQUEST);
        challengeBox = new JComboBox(challanges);
        challengeBox.setName(Strings.SEND_CHALLENGE_BUTTON_NAME);;
        panel.add(challengeBox,top);
        JButton sendChallenge = new JButton(Strings.SEND_BUTTON_TITTLE);
        sendChallenge.setName(Strings.SEND_CHALLENGE_BUTTON_NAME);
        sendChallenge.addActionListener(new buttonListener());
        panel.add(sendChallenge,top);
        verifierEditorPane =  new JEditorPane();
        verifierEditorPane.setEditable(false);
        JScrollPane editorScrollPane = new JScrollPane(verifierEditorPane);
        editorScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
        editorScrollPane.setPreferredSize(new Dimension(300, 250));
        editorScrollPane.setMinimumSize(new Dimension(300, 250));
        verifierEditorPane.setText(Strings.VERIFIER_RESPONSE_PLACEHOLDER);
        panel1.add(editorScrollPane, bottom);
        panel.revalidate();
        frame.add(panel1, BorderLayout.CENTER);
        frame.pack();
    }

    /**
     * This method repaints the prover GUI once all the
     * fields are filled and the Ok button is pressed. The
     * prover program then notifies the verifier and waits
     * for a challenge to be sent to it.
     */
    public void setupProverGUI(){
        this.panel.removeAll();
        JPanel panel1 = new JPanel();
        frame.setResizable(false);
        frame.setPreferredSize(new Dimension(300, 300));
        panel1.setLayout(new GridBagLayout());
        panel1.setLayout(new GridBagLayout());
        frame.getContentPane().add(panel, BorderLayout.NORTH);
        GridBagConstraints top = new GridBagConstraints();
        top.anchor = GridBagConstraints.FIRST_LINE_START;
        top.weightx = 1.0;
        top.weighty =1;
        this.notificationLabel.setText(Strings.CHALLENGE_RCVD);
        panel.add(notificationLabel,top);
        proverEditorPane =  new JEditorPane();
        proverEditorPane.setEditable(false);
        JScrollPane editorScrollPane = new JScrollPane(proverEditorPane);
        editorScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
        editorScrollPane.setPreferredSize(new Dimension(300, 250));
        editorScrollPane.setMinimumSize(new Dimension(300, 250));
        proverEditorPane.setText(Strings.PROVER_RESPONSE_PLACEHOLDER);
        panel1.add(editorScrollPane, top);
        panel.revalidate();
        frame.add(panel1, BorderLayout.CENTER);
        frame.pack();
    }

    /**
     * This method contains the routine to setup the GUI
     * for the verifier program. It positions the containers
     * and adds the graphical components to the container
     * to build the GUI.
     */
    public void initVerifier(){
        this.panel.removeAll();
        JPanel panel1 = new JPanel();
        panel1.setLayout(new BoxLayout(panel1, BoxLayout.PAGE_AXIS));
        panel1.setAlignmentX(Component.LEFT_ALIGNMENT);
        frame.setResizable(false);
        frame.setPreferredSize(new Dimension(300, 300));
        frame.setTitle(Strings.VERIFIER_TITLE);
        this.notificationLabel.setText(Strings.VERIFIER_LABEL);
        panel1.add(notificationLabel);
        panel1.add(Box.createRigidArea(new Dimension(0,10)));
        portField =  new JTextField();
        portField.setForeground(Color.lightGray);
        portField.setText(Strings.PORT_NUMBER_LABEL);
        portField.setMaximumSize(new Dimension(100, 20));
        portField.setPreferredSize(new Dimension(100, 20));
        portField.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                portField.setText("");
                portField.setForeground(Color.black);
            }
        });
        portField.setText(Strings.PORT_NUMBER_LABEL);
        panel1.add(portField);
        panel1.add(Box.createRigidArea(new Dimension(0, 10)));
        graph1Field =  new JTextField();
        graph1Field.setForeground(Color.lightGray);
        graph1Field.setText(Strings.GRAPH_ONE_FIELD_DEFAULT_TEXT);
        graph1Field.setName(Strings.GRAPH_ONE_FIELD_DEFAULT_NAME);
        graph1Field.setEditable(false);
        panel1.add(graph1Field);
        JButton openG1 = new JButton(Strings.OPEN);
        openG1.setName(Strings.GRAPH_ONE_BUTTON_NAME);
        openG1.addActionListener(new buttonListener());
        panel1.add(openG1);
        panel1.add(Box.createRigidArea(new Dimension(0,10)));
        graph2Field =  new JTextField();
        graph2Field.setForeground(Color.lightGray);
        graph2Field.setText(Strings.GRAPH_TWO_FIELD_DEFAULT_TEXT);
        graph2Field.setName(Strings.GRAPH_TWO_FIELD_DEFAULT_NAME);
        graph2Field.setEditable(false);
        panel1.add(graph2Field);
        JButton openG2 = new JButton(Strings.OPEN);
        openG2.setName(Strings.GRAPH_TWO_BUTTON_NAME);
        openG2.addActionListener(new buttonListener());
        panel1.add(openG2);
        panel1.add(Box.createRigidArea(new Dimension(0, 30)));
        JButton ok = new JButton(Strings.OK);
        ok.setName(Strings.VERIFIER_OK);
        ok.addActionListener(new buttonListener());
        panel1.add(ok);
        panel.revalidate();
        frame.add(panel1, BorderLayout.NORTH);
        frame.pack();

    }

    /**
     * This method contain the routine to setup the GUI
     * for the prover program. It positions the containers
     * and adds the graphical components to the container
     * to build the GUI.
     */
    public void initProver(){
        this.panel.removeAll();
        JPanel panel1 = new JPanel();
        panel1.setLayout(new BoxLayout(panel1, BoxLayout.PAGE_AXIS));
        panel1.setAlignmentX(Component.LEFT_ALIGNMENT);
        frame.setResizable(false);
        frame.setPreferredSize(new Dimension(300, 400));
        frame.setTitle(Strings.PROVER_TITLE);
        this.notificationLabel.setText(Strings.VERIFIER_LABEL);
        panel1.add(notificationLabel);
        panel1.add(Box.createRigidArea(new Dimension(0,10)));
        hostField =  new JTextField();
        hostField.setForeground(Color.lightGray);
        hostField.setText(Strings.HOST_TITLE);
        hostField.setMaximumSize(new Dimension(300, 20));
        hostField.setPreferredSize(new Dimension(100, 20));
        hostField.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                hostField.setText("");
                hostField.setForeground(Color.black);
            }
        });
        hostField.setText(Strings.HOST_PLACEHOLDER);
        panel1.add(hostField);
        panel1.add(Box.createRigidArea(new Dimension(0, 10)));
        portField =  new JTextField();
        portField.setForeground(Color.lightGray);
        portField.setText(Strings.PORT_NUMBER_LABEL);
        portField.setMaximumSize(new Dimension(100, 20));
        portField.setPreferredSize(new Dimension(100, 20));
        portField.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                portField.setText("");
                portField.setForeground(Color.black);
            }
        });
        panel1.add(portField);
        portField.setText(Strings.PORT_NUMBER_LABEL);
        panel1.add(Box.createRigidArea(new Dimension(0, 10)));
        graph1Field =  new JTextField();
        graph1Field.setForeground(Color.lightGray);
        graph1Field.setText(Strings.GRAPH_ONE_FIELD_DEFAULT_TEXT);
        graph1Field.setName(Strings.GRAPH_ONE_FIELD_DEFAULT_NAME);
        graph1Field.setEditable(false);
        panel1.add(graph1Field);
        JButton openG1 = new JButton(Strings.OPEN);
        openG1.setName(Strings.GRAPH_ONE_BUTTON_NAME);
        openG1.addActionListener(new buttonListener());
        panel1.add(openG1);
        panel1.add(Box.createRigidArea(new Dimension(0,10)));
        graph2Field =  new JTextField("");
        graph2Field.setForeground(Color.lightGray);
        graph2Field.setText(" add path to graph 2");
        graph2Field.setName(Strings.GRAPH_TWO_FIELD_DEFAULT_TEXT);
        graph2Field.setEditable(false);
        panel1.add(graph2Field);
        JButton openG2 = new JButton(Strings.OPEN);
        openG2.setName(Strings.GRAPH_TWO_BUTTON_NAME);
        openG2.addActionListener(new buttonListener());
        panel1.add(openG2);
        panel1.add(Box.createRigidArea(new Dimension(0,10)));
        piField =  new JTextField();
        piField.setForeground(Color.lightGray);
        piField.setText(Strings.GRAPH_PI_FIELD_DEFAULT_TEXT);
        piField.setName(Strings.GRAPH_TWO_FIELD_DEFAULT_NAME);
        piField.setEditable(false);
        panel1.add(piField);
        JButton openPi = new JButton(Strings.OPEN);
        openPi.setName(Strings.PI_BUTTON_NAME);
        openPi.addActionListener(new buttonListener());
        panel1.add(openPi);
        panel1.add(Box.createRigidArea(new Dimension(0, 30)));
        JButton ok = new JButton(Strings.OK);
        ok.setName(Strings.PROVER_OK);
        ok.addActionListener(new buttonListener());
        panel1.add(ok);
        panel.revalidate();
        frame.add(panel1, BorderLayout.NORTH);
        frame.pack();
    }

    /**
     * This method presents the user with the option
     * to re-initialize the gui and then re-initializes it
     * if the 'yes' option is chosen or quits if 'no' is chosen.
     */
    public RetryOption reInitialize(){
        if (JOptionPane.showConfirmDialog(null,Strings.TRY_AGAIN_MSG, " ",
            JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
            return RetryOption.YES;
        } 
        else {
            return RetryOption.NO;
        }
    }
}
