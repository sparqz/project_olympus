package edu.purdue.cs555;

import java.io.*;
import java.net.*;
import java.util.ArrayList;
import javax.crypto.spec.*;

/**
 * Class providing verifier functionality, here 
 * the commitment is recieved from the prover, 
 * the challenge is sent to the prover and the 
 * challenge response is evaluated. The verifier 
 * class is run as a secondary thread in order 
 * to seperate concerns from painting GUI and 
 * handling user events.
 */
public class Verifier implements Runnable{

	public  TestClient testClient = new TestClient();
	private ServerSocket sock;
	private Socket connectionSocket;
	private int port;
	private DataOutputStream outToClient;
	private ObjectInputStream objectInput;
	private Graph largeGraph;
	private Graph smallGraph;
	private Graph commitmentGraph;
	private SecretKeySpec commitmentKeys[][];
	private Graph permutationGraph;
	private String log = new String();

	/**
	 * This constructor takes the path to our graph 
	 * and sub-graph and the port on which to listen 
	 * for the prover's connection.
	 */
	public Verifier( String largeGraphFile, String smallGraphFile, int listenPort){
		this.largeGraph = new Graph();
		this.largeGraph.importGraph(largeGraphFile);
		this.smallGraph = new Graph();
		this.smallGraph.importGraph(smallGraphFile);
		this.port = listenPort;
		try {
			this.sock = new ServerSocket(port);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * This method sets up the network varialbes to read 
	 * input from and write to the prover program, it also 
	 * initiates the routine to setup verfier GUI.
	 */
	public void setup(){
		try {
			connectionSocket = sock.accept();
			objectInput = new ObjectInputStream(connectionSocket.getInputStream());
			outToClient = new DataOutputStream(connectionSocket.getOutputStream());
			this.testClient.setupVerifierGUI();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * This method carries out the actual verification of the 
	 * validity of the provers response to the challenges. It 
	 * receives the challenge type ( 1 or 2) as an argument 
	 * and checks for sub-graphism or isomorphism based on 
	 * challange that was made.
	 */
	public boolean verify(String challenge_type ){
		boolean result;
		if( challenge_type == null){
			return false;
		}
		commitmentGraph.decrypt(commitmentKeys);
		Graph G_possible = commitmentGraph.getIsomorphicMat(permutationGraph);
		if( challenge_type.equals("1")){
			result = G_possible.compare(largeGraph, true );

		}else{
			result = G_possible.compare(smallGraph, false );
		}
		try {
			if( result ){
				outToClient.writeBytes(Strings.OK+"\n");
				Thread.sleep(30);
				log+= "CHALLENGE #"+(testClient.challengeBox.getSelectedIndex()+1)+" PASSED\n";
				TestClient.verifierEditorPane.setText(log);
			}else{
				outToClient.writeBytes(Strings.NOT_OK + "\n");
				Thread.sleep(30);
				log+= "CHALLENGE #"+(testClient.challengeBox.getSelectedIndex()+1)+" FAILED\n";
				TestClient.verifierEditorPane.setText(log);
			}
		} catch (IOException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		return result;
	}

	/**
	 * This method sends the challenge to the prover (who will 
	 * be waiting at that point) and also receives the provers 
	 * commitment graph (encrypted adj-matrix), the permutation 
	 * and the keys to open some encrypted positions of the 
	 * commitment matrix. Encryption is used to guard the secret
	 * from the verifier.
	 */
	public boolean sendChallenge(int challenge){
		try{
			try {
				outToClient.writeBytes(challenge + "\n");
			} catch (Exception e) {
				e.printStackTrace();
			}
			try {
				Object serialized =objectInput.readObject();
				ArrayList<Object> objects = (ArrayList<Object>)serialized;
				commitmentGraph = (Graph) objects.get(0);
				permutationGraph = (Graph) objects.get(1);
				commitmentKeys = (SecretKeySpec[][]) objects.get(2);
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return verify(String.valueOf(challenge));
	}

	/**
	 * This method continuously runs the verifier setup method so 
	 * that the verifier can keep waiting forthe next connection 
	 * from the prover.
	 */
	public void run(){
		while(true){
			setup();
		}
	}
}
