README

included:
All source files (src/edu/purdue/cs555/*.java)
this README
Compiled Jar files (out/verifier.jar, out/prover.jar)
Sample Graphs for testing the system (/sample_graphs/*)

===Olympus===

Olympus is a prototype authentication system built to demonstrate one use case (authentication) of a zero-Knowledge proof cryptographic protocol. A zero knowledge protocol or proof allows one party (the prover) to convince another party (the verifier) that a certain statement is true without revealing any additional information to the verifier on how to prove the statement. In other words the verifier after being convinced that the statement is true is not any closer to being able to provide their own proof of the statement to another verifier.  Authentication is a direct use case for this type of protocol as it eliminates the chances of a rogue verifier reproducing the authentication of a system, it also eliminates the need to store authentication data such as passwords (or hashes of them). 

The statement, also known as secret used in this program is sub-graph isomorphism.  The proof hinges on the fact that sub-graph isomorphism is an NP-Complete problem, this makes the task of finding an isomorphic sub-graph computationally infeasible and assures the verifier that the prover is legitimate if they can convey enough information to show they know the isomorphic subgraph of the given graph. 
The proof is carried out by challenges, if the prover can show that the provided graph is a subgraph or it is isomorphic (never both) multiple times, where the challenge is chosen randomly by the verifier, then the prover must know the secret.  Using this model security can be vastly increased by simply increasing the number of rounds the prover has to prove to be authenticated.

This program uses the JAMA Package for some matrix manipulation functionalities. 

The sources are provided, but for ease of a use a packaged jar file is also provided, the instruction to run the program is as follows:

1) Open the verifier.jar file in the root directory.
2) Select verifier.
1) Open the prover.jar file in the root directory.
2) Select prover.
4) Fill out the appropriate fields for both Verifier and Prover to establish a connection.  the Verifier acts as a server and should be started before the Prover.
5) Conduct rounds “proving the secret” (challenges) between the verifier and prover.
6) If all rounds are passed then the prover is authenticated, if not then the authentication fails.

The system is designed around complex graph analysis (subgraph isomorphism) and so the atual security hinges on producing inital graphs where one is a subgraph and isomorphic to the other and also producing the isomorphism function (in the form of a graph). In practice for a highly secure system on failure to prove knowledge of the subgraph or its isomorphism would be enough to deny a user authentication; however for purposes of this demo the graphs provided allow multiple failures, the graphs are also configured so that verification fails on one challenge and succeeds on the other.



